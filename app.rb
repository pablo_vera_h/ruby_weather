require 'sinatra'
require 'weather-api'

class WheaterApp < Sinatra::Base
  get '/' do
    @info = { 468739 => "Buenos Aires",  418440 => "Lima", 349859 => "Santiago", 2459115 => "New York", 4118 => "Toronto" }
    @cities = Hash.new
    @info.each do |key,value|
      @cities[value] = Weather.lookup("#{key}", Weather::Units::CELSIUS)
    end
    slim :index
  end
end